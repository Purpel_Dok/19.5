﻿#include <iostream>
#include <string>
using namespace std;

class Animals
{
public:
    virtual void Voice()
    {
        cout << "Animal sound \n";
    }
};
class Dog : public Animals
{
public:
    void Voice() override{}
    Dog()
    {
        cout << "WOOF!! \n";
    } 
};
class Cat : public Animals
{
public:
    void Voice() override {}
    Cat()
    {
        cout << "MEAW!! \n";
    }
};
class Duck : public Animals
{
public:
    void Voice() override {}
    Duck()
    {
        cout << "QUACK!! \n";
    }  
};
class Cow : public Animals
{
public:
    void Voice() override {}
    Cow()
    {
        cout << "MOO!! \n";
    }
};

int main()
{   
    Animals* animals[4];
        animals[0] = new Dog();
        animals[1] = new Cat();
        animals[2] = new Duck();
        animals[3] = new Cow();

        for (int i = 0; i < 4; i++)
        { 
            animals[i]->Voice();
        }
        delete animals[0];
        delete animals[1];
        delete animals[2];
        delete animals[3];
    return 0;   
}

